#SimpleArray

This project is a simple implementation of an immutable array class cluster in Objective-C. It provides initialisation, indexed subscripting and counting. Such are the primitives from which a more full-features array implementation (pull requests welcome!) could be built.

## Licence

MIT. See COPYING.