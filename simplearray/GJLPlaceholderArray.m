// See COPYING for licence details.

#import "GJLPlaceholderArray.h"
#import "GJLRealArray.h"
#import <objc/runtime.h>

@implementation GJLPlaceholderArray

- (instancetype)initWithObjects:(id [])objects count:(NSUInteger)count
{
    GJLRealArray *returnedArray = class_createInstance([GJLRealArray class], count * sizeof(id));
    returnedArray->_count = count;
    id *indexedIvars = object_getIndexedIvars(returnedArray);
    for (NSUInteger i = 0; i < count; i++)
    {
        indexedIvars[i] = [objects[i] retain];
    }
    return (GJLPlaceholderArray *)returnedArray;
}

@end
