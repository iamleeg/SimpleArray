// See COPYING for licence details.

#import "GJLRealArray.h"
#import <objc/runtime.h>

@implementation GJLRealArray

- (NSUInteger)count
{
    return _count;
}

- (id)objectAtIndexedSubscript:(NSUInteger)index
{
    NSParameterAssert(index < [self count]);
    id *indexedIvars = object_getIndexedIvars(self);
    return [[indexedIvars[index] retain] autorelease];
}

- (NSString *)description
{
    NSMutableString *description = [@"SimpleArray( " mutableCopy];
    for (NSUInteger i = 0; i < [self count]; i++)
    {
        [description appendFormat:@"%@ ", self[i]];
    }
    [description appendString:@")"];
    return [description autorelease];
}

- (void)dealloc
{
    id *indexedIvars = object_getIndexedIvars(self);
    for (NSUInteger i = 0; i < _count; i++)
    {
        [indexedIvars[i] release];
    }
    [super dealloc];
}

@end