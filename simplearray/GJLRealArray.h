// See COPYING for licence details.

#import "GJLSimpleArray.h"

@interface GJLRealArray : GJLSimpleArray
{
@public
    NSUInteger _count;
}
@end
