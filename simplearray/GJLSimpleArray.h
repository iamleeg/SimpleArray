// See COPYING for licence details.

#import <Foundation/Foundation.h>

@interface GJLSimpleArray : NSObject

- (instancetype)initWithObjects:(__strong id[])objects count:(NSUInteger)count;
- (NSUInteger)count;
- (id)objectAtIndexedSubscript:(NSUInteger)index;

@property (nonatomic, assign, getter = isAwesome) BOOL awesome;

@end
