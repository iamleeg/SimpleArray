// See COPYING for licence details.

#import <Foundation/Foundation.h>
#import "GJLSimpleArray.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        id words[] = { @"Hello", @"London", @"tech", @"talk" };
        GJLSimpleArray *array = [[GJLSimpleArray alloc] initWithObjects:words count:4];
        NSLog(@"%@", array);
    }
    return 0;
}

