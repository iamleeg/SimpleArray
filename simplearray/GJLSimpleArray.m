// See COPYING for licence details.

#import "GJLSimpleArray.h"
#import "GJLPlaceholderArray.h"
#import <objc/runtime.h>

static GJLPlaceholderArray *placeholder;

@implementation GJLSimpleArray

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        placeholder = class_createInstance([GJLPlaceholderArray class], 0);
    });
    return placeholder;
}

- (instancetype)initWithObjects:(id [])objects count:(NSUInteger)count
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Method called on abstract instance" userInfo:nil];
}

- (NSUInteger)count
{
    return 0;
}

- (id)objectAtIndexedSubscript:(NSUInteger)index
{
    @throw [NSException exceptionWithName:NSRangeException reason:@"No objects" userInfo:nil];
}

@end
